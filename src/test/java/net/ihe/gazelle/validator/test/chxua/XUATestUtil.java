package net.ihe.gazelle.validator.test.chxua;

import net.ihe.gazelle.saml.assertion.AssertionType;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.validator.test.common.AbstractValidator;
import net.ihe.gazelle.chxua.validator.chxua.CHXUAPackValidator;

import java.util.List;

public class XUATestUtil extends AbstractValidator<AssertionType> {

    @Override
    protected void validate(AssertionType message,
                            List<Notification> notifications) {
        AssertionType.validateByModule(message, "/AssertionType", new CHXUAPackValidator(), notifications);
    }

    @Override
    protected Class<AssertionType> getMessageClass() {
        return AssertionType.class;
    }
}