package net.ihe.gazelle.validator.test.chxua;

import org.junit.Assert;
import org.junit.Test;

public class CHXUA_AssertionRoleREPTest {

	XUATestUtil testExecutor = new XUATestUtil();

	@Test
	public void test_ok_constraint_xua_NameID() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnValidFile(
						"src/test/resources/CH-XUA_REP_OK.xml",
						"chxua-CHXUA_AssertionRoleREP-ch_xua_042_NameID"));
	}

	@Test
	public void test_ko_constraint_xua_NameID() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnNonValidFile(
						"src/test/resources/CH-XUA_REP_KO.xml",
						"chxua-CHXUA_AssertionRoleREP-ch_xua_042_NameID"));
	}

	@Test
	public void test_ko_constraint_xua_PurposeOfUse() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnNonValidFile(
						"src/test/resources/CH-XUA_REP_KO.xml",
						"chxua-CHXUA_AssertionRoleREP-ch_xua_097_PurposeOfUse"));
	}

	@Test
	public void test_ok_constraint_xua_PurposeOfUse() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnValidFile(
						"src/test/resources/CH-XUA_REP_OK.xml",
						"chxua-CHXUA_AssertionRoleREP-ch_xua_097_PurposeOfUse"));
	}
}		
