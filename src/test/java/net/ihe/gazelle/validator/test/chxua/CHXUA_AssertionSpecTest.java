package net.ihe.gazelle.validator.test.chxua;

import org.junit.Assert;
import org.junit.Test;

public class CHXUA_AssertionSpecTest {

   XUATestUtil testExecutor = new XUATestUtil();


   @Test
   // The User Assertion MUST contain the attribute: /AttributeStatement/Attribute[@Name="urn:oasis:names:tc:xspa:1.0:subject:subject-id"]. The
   // AttributeValue MUST be the plain text of the users name (e.g., "John Doe"). NOK
   public void test_ko_constraint_xua_subjectID() {
      Assert.assertTrue(testExecutor
            .checkConstraintOnNonValidFile(
                  "src/test/resources/CH-XUA-002_NoSubjectID.xml",
                  "chxua-CHXUA_AssertionSpec-ch_xua_002_SubjectID"));
   }

   @Test
   // The User Assertion MUST contain the attribute: /AttributeStatement/Attribute[@Name="urn:oasis:names:tc:xspa:1.0:subject:subject-id"]. The
   // AttributeValue MUST be the plain text of the users name (e.g., "John Doe"). NOK
   public void test_ko_constraint_xua_subjectID_duplicated() {
      Assert.assertTrue(testExecutor
              .checkConstraintOnNonValidFile(
                      "src/test/resources/CH-XUA_KO_duplicated.xml",
                      "chxua-CHXUA_AssertionSpec-ch_xua_002_SubjectID"));
   }

   @Test
   // The User Assertion MUST contain the attribute: /AttributeStatement/Attribute[@Name="urn:oasis:names:tc:xspa:1.0:subject:subject-id"]. The
   // AttributeValue MUST be the plain text of the users name (e.g., "John Doe"). NOK
   public void test_ko_constraint_xua_subjectID_duplicated_value() {
      Assert.assertTrue(testExecutor
              .checkConstraintOnNonValidFile(
                      "src/test/resources/CH-XUA_KO_duplicated_value.xml",
                      "chxua-CHXUA_AssertionSpec-ch_xua_002_SubjectID"));
   }

   @Test
   // The User Assertion MUST contain the attribute: /AttributeStatement/Attribute[@Name="urn:oasis:names:tc:xspa:1.0:subject:subject-id"]. The
   // AttributeValue MUST be the plain text of the users name (e.g., "John Doe"). OK
   public void test_ok_constraint_xua_subjectID() {
      Assert.assertTrue(testExecutor
            .checkConstraintOnValidFile(
                  "src/test/resources/CH-XUA_OK.xml",
                  "chxua-CHXUA_AssertionSpec-ch_xua_002_SubjectID"));
   }

   @Test
   // The User Assertion MUST contain the attribute: /AttributeStatement/Attribute[@name=”urn:oasis:names:tc:xacml:2.0:subject:role”]. The
   // AttributeValue MUST be PAT for patient, HCP for healthcare professional, ASS for Assistant or REP for Representative. NOK
   public void test_ko_constraint_xua_role() {
      Assert.assertTrue(testExecutor
            .checkConstraintOnNonValidFile(
                  "src/test/resources/CH-XUA-005_NoRole.xml",
                  "chxua-CHXUA_AssertionSpec-ch_xua_005_Role"));
   }

   @Test
   // The User Assertion MUST contain the attribute: /AttributeStatement/Attribute[@name=”urn:oasis:names:tc:xacml:2.0:subject:role”]. The
   // AttributeValue MUST be PAT for patient, HCP for healthcare professional, ASS for Assistant or REP for Representative. NOK
   public void test_ko_constraint_xua_role_duplicated() {
      Assert.assertTrue(testExecutor
              .checkConstraintOnNonValidFile(
                      "src/test/resources/CH-XUA_KO_duplicated.xml",
                      "chxua-CHXUA_AssertionSpec-ch_xua_005_Role"));
   }

   @Test
   // The User Assertion MUST contain the attribute: /AttributeStatement/Attribute[@name=”urn:oasis:names:tc:xacml:2.0:subject:role”]. The
   // AttributeValue MUST be PAT for patient, HCP for healthcare professional, ASS for Assistant or REP for Representative. NOK
   public void test_ko_constraint_xua_role_duplicated_value() {
      Assert.assertTrue(testExecutor
              .checkConstraintOnNonValidFile(
                      "src/test/resources/CH-XUA_KO_duplicated_value.xml",
                      "chxua-CHXUA_AssertionSpec-ch_xua_005_Role"));
   }

   @Test
   // The User Assertion MUST contain the attribute: /AttributeStatement/Attribute[@name=”urn:oasis:names:tc:xacml:2.0:subject:role”]. The
   // AttributeValue MUST be PAT for patient, HCP for healthcare professional, ASS for Assistant or REP for Representative. OK
   public void test_ok_constraint_xua_role() {
      Assert.assertTrue(testExecutor
            .checkConstraintOnValidFile(
                  "src/test/resources/CH-XUA_OK.xml",
                  "chxua-CHXUA_AssertionSpec-ch_xua_005_Role"));
   }

   @Test
   // The User Assertion MUST contain the attribute: /AttributeStatement/Attribute[@name=”urn:oasis:names:tc:xacml:2.0:subject:role”]. The
   // AttributeValue MUST be PAT for patient, HCP for healthcare professional, ASS for Assistant or REP for Representative. OK
   public void test_ok_constraint_xua_role_PRO() {
      Assert.assertTrue(testExecutor
            .checkConstraintOnValidFile(
                  "src/test/resources/CH-XUA_OK.xml",
                  "chxua-CHXUA_AssertionSpec-ch_xua_005_Role"));
   }

   @Test
   // The User Assertion MUST contain the attribute: /AttributeStatement/Attribute[@name=”urn:oasis:names:tc:xacml:2.0:subject:role”]. The
   // AttributeValue MUST be PAT for patient, HCP for healthcare professional, ASS for Assistant or REP for Representative. NOK
   public void test_ko_constraint_xua_noResourcesID() {
      Assert.assertTrue(testExecutor
            .checkConstraintOnNonValidFile(
                  "src/test/resources/CH-XUA-006_NoResourceID.xml",
                  "chxua-CHXUA_AssertionSpec-ch_xua_006_ResourceID"));
   }

   @Test
   // The User Assertion MUST contain the attribute: /AttributeStatement/Attribute[@name=”urn:oasis:names:tc:xacml:2.0:subject:role”]. The
   // AttributeValue MUST be PAT for patient, HCP for healthcare professional, ASS for Assistant or REP for Representative. NOK
   public void test_ko_constraint_xua_noResourcesID_duplicated() {
      Assert.assertTrue(testExecutor
              .checkConstraintOnNonValidFile(
                      "src/test/resources/CH-XUA_KO_duplicated.xml",
                      "chxua-CHXUA_AssertionSpec-ch_xua_006_ResourceID"));
   }

   @Test
   // The User Assertion MUST contain the attribute: /AttributeStatement/Attribute[@name=”urn:oasis:names:tc:xacml:2.0:subject:role”]. The
   // AttributeValue MUST be PAT for patient, HCP for healthcare professional, ASS for Assistant or REP for Representative. NOK
   public void test_ko_constraint_xua_noResourcesID_duplicated_value() {
      Assert.assertTrue(testExecutor
              .checkConstraintOnNonValidFile(
                      "src/test/resources/CH-XUA_KO_duplicated_value.xml",
                      "chxua-CHXUA_AssertionSpec-ch_xua_006_ResourceID"));
   }

   @Test
   // The User Assertion MUST contain the attribute: /AttributeStatement/Attribute[@name=”urn:oasis:names:tc:xacml:2.0:resource:resource-id”]. The
   // AttributeValue MUST be the EPR-PID of the patient, to which the transaction refers. OK
   public void test_ok_constraint_xua_resourceID() {
      Assert.assertTrue(testExecutor
            .checkConstraintOnValidFile(
                  "src/test/resources/CH-XUA_OK.xml",
                  "chxua-CHXUA_AssertionSpec-ch_xua_006_ResourceID"));
   }


   @Test
   // The User Assertion MUST contain the attribute: /AttributeStatement/Attribute[@name=”urn:oasis:names:tc:xacml:2.0:subject:role”]. The
   // AttributeValue MUST be PAT for patient, HCP for healthcare professional, ASS for Assistant or REP for Representative. NOK
   public void test_ko_constraint_xua_purposeOfUse() {
      Assert.assertTrue(testExecutor
            .checkConstraintOnNonValidFile(
                  "src/test/resources/CH-XUA-007_NoPurposeOfUse.xml",
                  "chxua-CHXUA_AssertionSpec-ch_xua_007_PurposeOfUse"));
   }

   @Test
   // The User Assertion MUST contain the attribute: /AttributeStatement/Attribute[@name=”urn:oasis:names:tc:xacml:2.0:subject:role”]. The
   // AttributeValue MUST be PAT for patient, HCP for healthcare professional, ASS for Assistant or REP for Representative. NOK
   public void test_ko_constraint_xua_purposeOfUse_duplicated() {
      Assert.assertTrue(testExecutor
              .checkConstraintOnNonValidFile(
                      "src/test/resources/CH-XUA_KO_duplicated.xml",
                      "chxua-CHXUA_AssertionSpec-ch_xua_007_PurposeOfUse"));
   }

   @Test
   // The User Assertion MUST contain the attribute: /AttributeStatement/Attribute[@name=”urn:oasis:names:tc:xacml:2.0:subject:role”]. The
   // AttributeValue MUST be PAT for patient, HCP for healthcare professional, ASS for Assistant or REP for Representative. NOK
   public void test_ko_constraint_xua_purposeOfUse_duplicated_value() {
      Assert.assertTrue(testExecutor
              .checkConstraintOnNonValidFile(
                      "src/test/resources/CH-XUA_KO_duplicated_value.xml",
                      "chxua-CHXUA_AssertionSpec-ch_xua_007_PurposeOfUse"));
   }

   @Test
   // The User Assertion MUST contain the attribute: /AttributeStatement/Attribute[@name=”urn:oasis:names:tc:xacml:2.0:subject:role”]. The
   // AttributeValue MUST be PAT for patient, HCP for healthcare professional, ASS for Assistant or REP for Representative. OK
   public void test_ok_constraint_xua_purposeOfUse() {
      Assert.assertTrue(testExecutor
            .checkConstraintOnValidFile(
                  "src/test/resources/CH-XUA_OK.xml",
                  "chxua-CHXUA_AssertionSpec-ch_xua_007_PurposeOfUse"));
   }

   @Test
   // The User Assertion MUST contain the attribute: /AttributeStatement/Attribute[@name=”urn:oasis:names:tc:xacml:2.0:resource:resource-id”]. The
   // AttributeValue MUST be the EPR-PID of the patient, to which the transaction refers. NOK
   public void test_ko_constraint_xua_ResourcesID_EPRSPID() {
      Assert.assertTrue(testExecutor
            .checkConstraintOnNonValidFile(
                  "src/test/resources/CH-XUA-006_WrongResourceID.xml",
                  "chxua-CHXUA_AssertionSpec-ch_xua_008_ResourceID_EPR_SPID", "Warning"));
   }

   @Test
   // The User Assertion MUST contain the attribute: /AttributeStatement/Attribute[@name=”urn:oasis:names:tc:xacml:2.0:resource:resource-id”]. The
   // AttributeValue MUST be the EPR-PID of the patient, to which the transaction refers. NOK
   public void test_ko_constraint_xua_ResourcesID_duplicated() {
      Assert.assertTrue(testExecutor
              .checkConstraintOnNonValidFile(
                      "src/test/resources/CH-XUA_KO_duplicated.xml",
                      "chxua-CHXUA_AssertionSpec-ch_xua_008_ResourceID_EPR_SPID", "Warning"));
   }

   @Test
   // The User Assertion MUST contain the attribute: /AttributeStatement/Attribute[@name=”urn:oasis:names:tc:xacml:2.0:resource:resource-id”]. The
   // AttributeValue MUST be the EPR-PID of the patient, to which the transaction refers. NOK
   public void test_ko_constraint_xua_ResourcesID_duplicated_value() {
      Assert.assertTrue(testExecutor
              .checkConstraintOnNonValidFile(
                      "src/test/resources/CH-XUA_KO_duplicated_value.xml",
                      "chxua-CHXUA_AssertionSpec-ch_xua_008_ResourceID_EPR_SPID", "Warning"));
   }

   @Test
   // The User Assertion MUST contain the attribute: /AttributeStatement/Attribute[@name=”urn:oasis:names:tc:xacml:2.0:subject:role”]. The
   // AttributeValue MUST be PAT for patient, HCP for healthcare professional, ASS for Assistant or REP for Representative. OK
   public void test_ok_constraint_xua_ResourcesID_EPRSPID() {
      Assert.assertTrue(testExecutor
            .checkConstraintOnValidFile(
                  "src/test/resources/CH-XUA_OK.xml",
                  "chxua-CHXUA_AssertionSpec-ch_xua_008_ResourceID_EPR_SPID"));
   }

   @Test
   // There MUST be one <Attribute> element with the name attribute:"urn:ihe:iti:xca:2010:homeCommunityId". The <AttributeValue> child element MUST
   // convey the value of the Home Community ID (an Object Identifier) assigned to the Community that is initiating the request, using the urn
   // format (that is, “urn:oid:” appended with the OID). NOK
   public void test_ko_constraint_xua_homeCommunityID() {
      Assert.assertTrue(testExecutor
            .checkConstraintOnNonValidFile(
                  "src/test/resources/CH-XUA-099_NoHomeCommunityID.xml",
                  "chxua-CHXUA_AssertionSpec-ch_xua_099_homeCommunityID"));
   }

   @Test
   // There MUST be one <Attribute> element with the name attribute:"urn:ihe:iti:xca:2010:homeCommunityId". The <AttributeValue> child element MUST
   // convey the value of the Home Community ID (an Object Identifier) assigned to the Community that is initiating the request, using the urn
   // format (that is, “urn:oid:” appended with the OID). NOK
   public void test_ko_constraint_xua_homeCommunityID_duplicated() {
      Assert.assertTrue(testExecutor
              .checkConstraintOnNonValidFile(
                      "src/test/resources/CH-XUA_KO_duplicated.xml",
                      "chxua-CHXUA_AssertionSpec-ch_xua_099_homeCommunityID"));
   }

   @Test
   // There MUST be one <Attribute> element with the name attribute:"urn:ihe:iti:xca:2010:homeCommunityId". The <AttributeValue> child element MUST
   // convey the value of the Home Community ID (an Object Identifier) assigned to the Community that is initiating the request, using the urn
   // format (that is, “urn:oid:” appended with the OID). NOK
   public void test_ko_constraint_xua_homeCommunityID_duplicated_value() {
      Assert.assertTrue(testExecutor
              .checkConstraintOnNonValidFile(
                      "src/test/resources/CH-XUA_KO_duplicated_value.xml",
                      "chxua-CHXUA_AssertionSpec-ch_xua_099_homeCommunityID"));
   }

   @Test
   // There MUST be one <Attribute> element with the name attribute:"urn:ihe:iti:xca:2010:homeCommunityId". The <AttributeValue> child element MUST
   // convey the value of the Home Community ID (an Object Identifier) assigned to the Community that is initiating the request, using the urn
   // format (that is, “urn:oid:” appended with the OID). NOK
   public void test_ko_constraint_xua_homeCommunityID_OID() {
      Assert.assertTrue(testExecutor
            .checkConstraintOnNonValidFile(
                  "src/test/resources/CH-XUA-099_WrongHomeCommunityID.xml",
                  "chxua-CHXUA_AssertionSpec-ch_xua_099_homeCommunityID"));
   }

   @Test
   // There MUST be one <Attribute> element with the name attribute:"urn:ihe:iti:xca:2010:homeCommunityId". The <AttributeValue> child element MUST
   // convey the value of the Home Community ID (an Object Identifier) assigned to the Community that is initiating the request, using the urn
   // format (that is, “urn:oid:” appended with the OID). OK
   public void test_ok_constraint_xua_homeCommunityID() {
      Assert.assertTrue(testExecutor
            .checkConstraintOnValidFile(
                  "src/test/resources/CH-XUA_OK.xml",
                  "chxua-CHXUA_AssertionSpec-ch_xua_099_homeCommunityID"));
   }
}
