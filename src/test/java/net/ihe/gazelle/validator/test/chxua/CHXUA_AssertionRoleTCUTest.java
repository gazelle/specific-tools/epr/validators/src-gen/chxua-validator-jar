package net.ihe.gazelle.validator.test.chxua;

import org.junit.Assert;
import org.junit.Test;

public class CHXUA_AssertionRoleTCUTest {

    XUATestUtil testExecutor = new XUATestUtil();

    @Test
    public void test_ko_constraint_xua_AudienceRestrication() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/CH-XUA_TCU_KO.xml",
                        "chxua-CHXUA_AssertionRoleTCU-ch_xua_035_AudienceRest"));
    }

    @Test
    public void test_ok_constraint_xua_AudienceRestrication() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/CH-XUA_TCU_OK.xml",
                        "chxua-CHXUA_AssertionRoleTCU-ch_xua_035_AudienceRest"));
    }

    @Test
    public void test_ko_constraint_xua_Delegation() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/CH-XUA_TCU_KO.xml",
                        "chxua-CHXUA_AssertionRoleTCU-ch_xua_089_Delegation"));
    }

    @Test
    public void test_ok_constraint_xua_Delegation() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/CH-XUA_TCU_OK.xml",
                        "chxua-CHXUA_AssertionRoleTCU-ch_xua_089_Delegation"));
    }

    @Test
    public void test_ko_constraint_xua_PurposeOfUse() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/CH-XUA_TCU_KO.xml",
                        "chxua-CHXUA_AssertionRoleTCU-ch_xua_093_PurposeOfUse"));
    }

    @Test
    public void test_ok_constraint_xua_PurposeOfUse() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/CH-XUA_TCU_OK.xml",
                        "chxua-CHXUA_AssertionRoleTCU-ch_xua_093_PurposeOfUse"));
    }

    @Test
    public void test_ok_constraint_xua_PurposeOfUse_dicom() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/CH-XUA_TCU_OK_DICOM_AUTO.xml",
                        "chxua-CHXUA_AssertionRoleTCU-ch_xua_093_PurposeOfUse"));
    }
}		
