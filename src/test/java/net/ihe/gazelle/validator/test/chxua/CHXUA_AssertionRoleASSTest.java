package net.ihe.gazelle.validator.test.chxua;

import org.junit.Assert;
import org.junit.Test;

public class CHXUA_AssertionRoleASSTest {

    XUATestUtil testExecutor = new XUATestUtil();

    @Test
    public void test_ko_constraint_xua_AudienceRestrication() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/CH-XUA_ASS_KO.xml",
                        "chxua-CHXUA_AssertionRoleASS-ch_xua_035_AudienceRestriction"));
    }

    @Test
    public void test_ok_constraint_xua_AudienceRestrication() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/CH-XUA_ASS_OK.xml",
                        "chxua-CHXUA_AssertionRoleASS-ch_xua_035_AudienceRestriction"));
    }

    @Test
    public void test_ko_constraint_xua_Delegation() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/CH-XUA_ASS_KO.xml",
                        "chxua-CHXUA_AssertionRoleASS-ch_xua_088_Delegation"));
    }

    @Test
    public void test_ok_constraint_xua_Delegation() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/CH-XUA_ASS_OK.xml",
                        "chxua-CHXUA_AssertionRoleASS-ch_xua_088_Delegation"));
    }

    @Test
    public void test_ko_constraint_xua_PurposeOfUse() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/CH-XUA_ASS_KO.xml",
                        "chxua-CHXUA_AssertionRoleASS-ch_xua_097_PurposeOfUse"));
    }

    @Test
    public void test_ok_constraint_xua_PurposeOfUse() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/CH-XUA_ASS_OK.xml",
                        "chxua-CHXUA_AssertionRoleASS-ch_xua_097_PurposeOfUse"));
    }
}		
