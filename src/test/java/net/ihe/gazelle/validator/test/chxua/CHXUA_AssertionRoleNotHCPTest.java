package net.ihe.gazelle.validator.test.chxua;

import org.junit.Assert;
import org.junit.Test;

public class CHXUA_AssertionRoleNotHCPTest {

	XUATestUtil testExecutor = new XUATestUtil();

	@Test
	// The User Assertion MUST contain the attribute: /AttributeStatement/Attribute[@Name="urn:oasis:names:tc:xspa:1.0:subject:organization-id"]
	// The AttributeValue MUST be for healthcare professionals: GLN of an organization or a group from the Health Organization Index (HOI) and for
	// patients: empty NOK
	public void test_ok_constraint_xua_OrganizationID() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnValidFile(
						"src/test/resources/CH-XUA_Not_HCP_OK.xml",
						"chxua-CHXUA_AssertionRoleNotHCP-ch_xua_038_OrganizationId"));
	}

	@Test
	// The User Assertion MUST contain the attribute: /AttributeStatement/Attribute[@Name="urn:oasis:names:tc:xspa:1.0:subject:organization-id"]
	// The AttributeValue MUST be for healthcare professionals: GLN of an organization or a group from the Health Organization Index (HOI) and for
	// patients: empty/ OK
	public void test_ko_constraint_xua_organizationID() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnNonValidFile(
						"src/test/resources/CH-XUA_Not_HCP_KO.xml",
						"chxua-CHXUA_AssertionRoleNotHCP-ch_xua_038_OrganizationId"));
	}

	@Test
	// The User Assertion MUST contain the attribute: /AttributeStatement/Attribute[@Name="urn:oasis:names:tc:xspa:1.0:subject:organization"]. the
	// AttributeValue MUST be for healthcare professionals: Plain text of the organizations name and for patients: empty. NOK
	public void test_ko_constraint_xua_Organization() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnNonValidFile(
						"src/test/resources/CH-XUA_Not_HCP_KO.xml",
						"chxua-CHXUA_AssertionRoleNotHCP-ch_xua_039_Organization"));
	}

	@Test
	// The User Assertion MUST contain the attribute: /AttributeStatement/Attribute[@Name="urn:oasis:names:tc:xspa:1.0:subject:organization"]. the
	// AttributeValue MUST be for healthcare professionals: Plain text of the organizations name and for patients: empty. OK
	public void test_ok_constraint_xua_organization() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnValidFile(
						"src/test/resources/CH-XUA_Not_HCP_OK.xml",
						"chxua-CHXUA_AssertionRoleNotHCP-ch_xua_039_Organization"));
	}

}		
