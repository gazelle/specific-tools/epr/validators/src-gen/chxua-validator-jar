package net.ihe.gazelle.validator.test.chxua;

import org.junit.Assert;
import org.junit.Test;

public class CHXUA_AssertionRoleDADMTest {

	XUATestUtil testExecutor = new XUATestUtil();

	@Test
	public void test_ko_constraint_xua_NameID() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnNonValidFile(
						"src/test/resources/CH-XUA_DADM_KO.xml",
						"chxua-CHXUA_AssertionRoleDADM-ch_xua_037_NameID"));
	}

	@Test
	public void test_ok_constraint_xua_NameID() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnValidFile(
						"src/test/resources/CH-XUA_DADM_OK.xml",
						"chxua-CHXUA_AssertionRoleDADM-ch_xua_037_NameID"));
	}

	@Test
	public void test_ko_constraint_xua_PurposeOfUse() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnNonValidFile(
						"src/test/resources/CH-XUA_DADM_KO.xml",
						"chxua-CHXUA_AssertionRoleDADM-ch_xua_095_PurposeOfUse"));
	}

	@Test
	public void test_ok_constraint_xua_PurposeOfUse() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnValidFile(
						"src/test/resources/CH-XUA_DADM_OK.xml",
						"chxua-CHXUA_AssertionRoleDADM-ch_xua_095_PurposeOfUse"));
	}
}		
