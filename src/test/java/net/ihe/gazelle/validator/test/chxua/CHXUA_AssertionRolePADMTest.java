package net.ihe.gazelle.validator.test.chxua;

import org.junit.Assert;
import org.junit.Test;

public class CHXUA_AssertionRolePADMTest {

	XUATestUtil testExecutor = new XUATestUtil();

	@Test
	public void test_ko_constraint_xua_NameID() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnNonValidFile(
						"src/test/resources/CH-XUA_PADM_KO.xml",
						"chxua-CHXUA_AssertionRolePADM-ch_xua_036_NameID"));
	}

	@Test
	public void test_ok_constraint_xua_NameID() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnValidFile(
						"src/test/resources/CH-XUA_PADM_OK.xml",
						"chxua-CHXUA_AssertionRolePADM-ch_xua_036_NameID"));
	}

	@Test
	public void test_ko_constraint_xua_PurposeOfUse() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnNonValidFile(
						"src/test/resources/CH-XUA_PADM_KO.xml",
						"chxua-CHXUA_AssertionRolePADM-ch_xua_094_PurposeOfUse"));
	}

	@Test
	public void test_ok_constraint_xua_PurposeOfUse() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnValidFile(
						"src/test/resources/CH-XUA_PADM_OK.xml",
						"chxua-CHXUA_AssertionRolePADM-ch_xua_094_PurposeOfUse"));
	}
}		
