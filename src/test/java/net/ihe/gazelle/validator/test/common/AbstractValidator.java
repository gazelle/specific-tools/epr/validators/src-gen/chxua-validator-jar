package net.ihe.gazelle.validator.test.common;

import net.ihe.gazelle.saml.assertion.AssertionType;
import net.ihe.gazelle.tran.test.Transform;
import net.ihe.gazelle.validation.Notification;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractValidator<T> {

    public boolean checkConstraintOnValidFile(String filePath, String constraintName) {
        return checkConstraint(filePath, constraintName, "Note");
    }

    public boolean checkConstraintOnNonValidFile(String filePath, String constraintName) {
        return checkConstraint(filePath, constraintName, "Error");
    }

    public boolean checkConstraintOnNonValidFile(String filePath, String constraintName, String constraintType) {
        return checkConstraint(filePath, constraintName, constraintType);
    }

    protected abstract void validate(AssertionType message, List<Notification> notifications);

    protected abstract Class<T> getMessageClass();

    private boolean checkConstraint(String filePath, String constraintName, String notificationType) {
        try {
            AssertionType message = Transform.load(new FileInputStream(new File(filePath)));
            if (message == null) {
                return false;
            } else {
                List<Notification> notifications = new ArrayList<Notification>();
                validate(message, notifications);
                for (Notification notification : notifications) {
                    if (notification.getIdentifiant().equals(constraintName) && notification.getClass().getSimpleName()
                            .equals(notificationType)) {
                        return true;
                    } else if (notification.getIdentifiant().equals(constraintName)) {
                        System.out.println(notification.getIdentifiant() + ": " + notification.getDescription());
                        continue;
                    } else {
                        continue;
                    }
                }
                return false;
            }
        } catch (FileNotFoundException e) {
            System.out.println(filePath + " does not exist");
            return false;
        } catch (JAXBException e) {
            System.out.println("Unable to unmarshall message at " + filePath);
            return false;
        }
    }
}
