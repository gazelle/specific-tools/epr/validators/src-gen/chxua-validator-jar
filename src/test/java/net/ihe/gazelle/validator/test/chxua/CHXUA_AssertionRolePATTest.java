package net.ihe.gazelle.validator.test.chxua;

import org.junit.Assert;
import org.junit.Test;
public class CHXUA_AssertionRolePATTest {

	XUATestUtil testExecutor = new XUATestUtil();


	@Test
	public void test_ko_constraint_xua_nameID() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnNonValidFile(
						"src/test/resources/CH-XUA_PAT_KO.xml",
						"chxua-CHXUA_AssertionRolePAT-ch_xua_041_NameID"));
	}

	@Test
	public void test_ok_constraint_xua_nameID() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnValidFile(
						"src/test/resources/CH-XUA_PAT_OK.xml",
						"chxua-CHXUA_AssertionRolePAT-ch_xua_041_NameID"));
	}

	@Test
	public void test_ko_constraint_xua_PurposeOfUse() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnNonValidFile(
						"src/test/resources/CH-XUA_PAT_KO.xml",
						"chxua-CHXUA_AssertionRolePAT-ch_xua_096_PurposeOfUse"));
	}

	@Test
	public void test_ok_constraint_xua_PurposeOfUse() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnValidFile(
						"src/test/resources/CH-XUA_PAT_OK.xml",
						"chxua-CHXUA_AssertionRolePAT-ch_xua_096_PurposeOfUse"));
	}


}		
