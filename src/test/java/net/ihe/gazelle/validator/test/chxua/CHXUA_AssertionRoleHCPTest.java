package net.ihe.gazelle.validator.test.chxua;

import org.junit.Assert;
import org.junit.Test;

public class CHXUA_AssertionRoleHCPTest {

	XUATestUtil testExecutor = new XUATestUtil();

	@Test
	public void test_ok_constraint_xua_OrganizationID() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnValidFile(
						"src/test/resources/CH-XUA_HCP_OK.xml",
						"chxua-CHXUA_AssertionRoleHCP-ch_xua_033_OrganizationId"));
	}

	@Test
	public void test_ko_constraint_xua_organizationID() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnNonValidFile(
						"src/test/resources/CH-XUA_HCP_KO.xml",
						"chxua-CHXUA_AssertionRoleHCP-ch_xua_033_OrganizationId"));
	}

	@Test
	public void test_ko_constraint_xua_organizationID_duplicated() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnNonValidFile(
						"src/test/resources/CH-XUA_HCP_KO_duplicated.xml",
						"chxua-CHXUA_AssertionRoleHCP-ch_xua_033_OrganizationId"));
	}

	@Test
	public void test_ko_constraint_xua_Organization() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnNonValidFile(
						"src/test/resources/CH-XUA_HCP_KO.xml",
						"chxua-CHXUA_AssertionRoleHCP-ch_xua_032_Organization"));
	}

	@Test
	public void test_ko_constraint_xua_Organization_duplicated() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnNonValidFile(
						"src/test/resources/CH-XUA_HCP_KO_duplicated.xml",
						"chxua-CHXUA_AssertionRoleHCP-ch_xua_032_Organization"));
	}

	@Test
	public void test_ok_constraint_xua_organization() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnValidFile(
						"src/test/resources/CH-XUA_HCP_OK.xml",
						"chxua-CHXUA_AssertionRoleHCP-ch_xua_032_Organization"));
	}

	@Test
	public void test_ko_constraint_xua_SubjectNameID() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnNonValidFile(
						"src/test/resources/CH-XUA_HCP_KO.xml",
						"chxua-CHXUA_AssertionRoleHCP-ch_xua_031_SubjetNameID"));
	}

	@Test
	public void test_ok_constraint_xua_SubjectNameID() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnValidFile(
						"src/test/resources/CH-XUA_HCP_OK.xml",
						"chxua-CHXUA_AssertionRoleHCP-ch_xua_031_SubjetNameID"));
	}

    @Test
    public void test_ok_constraint_xua_SubjectConfHCP() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/CH-XUA_HCP_OK.xml",
                        "chxua-CHXUA_AssertionRoleHCP-ch_xua_034_SubjectConfNameID"));
    }

    @Test
    public void test_ko_constraint_xua_SubjectConfHCP() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/CH-XUA_HCP_KO.xml",
                        "chxua-CHXUA_AssertionRoleHCP-ch_xua_034_SubjectConfNameID"));
    }

    @Test
    public void test_ok_constraint_xua_SubjectConfASS() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/CH-XUA_ASS_OK.xml",
                        "chxua-CHXUA_AssertionRoleHCP-ch_xua_034_SubjectConfNameID"));
    }

    @Test
    public void test_ok_constraint_xua_SubjectConfTCU() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/CH-XUA_TCU_OK.xml",
                        "chxua-CHXUA_AssertionRoleHCP-ch_xua_034_SubjectConfNameID"));
    }

	@Test
	public void test_ko_constraint_xua_PurposeOfUse() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnNonValidFile(
						"src/test/resources/CH-XUA_HCP_KO.xml",
						"chxua-CHXUA_AssertionRoleHCP-ch_xua_098_PurposeOfUse"));
	}

	@Test
	public void test_ok_constraint_xua_PurposeOfUse() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnValidFile(
						"src/test/resources/CH-XUA_HCP_OK.xml",
						"chxua-CHXUA_AssertionRoleHCP-ch_xua_098_PurposeOfUse"));
	}

	@Test
	public void test_ok_constraint_xua_PurposeOfUse_TCU() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnValidFile(
						"src/test/resources/CH-XUA_TCU_OK.xml",
						"chxua-CHXUA_AssertionRoleHCP-ch_xua_098_PurposeOfUse"));
	}
}
