package net.ihe.gazelle.chxua.validator.chxua;

import net.ihe.gazelle.saml.assertion.AttributeStatementType;
import net.ihe.gazelle.saml.assertion.AttributeType;
import net.ihe.gazelle.saml.assertion.AttributeValueType;
import net.ihe.gazelle.saml.assertion.SubjectConfirmationType;
import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import java.util.List;


/**
 * class :        CHXUA_AssertionRoleHCP
 * package :   chxua
 * Template Class
 * Template identifier :
 * Class of test : AssertionSpec
 */
public final class CHXUA_AssertionRoleHCPValidator {


    private CHXUA_AssertionRoleHCPValidator() {
    }


    /**
     * Validation of instance by a constraint : ch_xua_031_SubjetNameID
     * In the Heathcare Professional Extension, the Assistant Extension and the Technical User Extension, The <NameID> child element of the <Subject> element MUST contain the GLN of the subject (responsible healthcare professional) with name qualifier attribute set to urn:gs1:gln.
     */
    private static boolean _validateCHXUA_AssertionRoleHCP_Ch_xua_031_SubjetNameID(net.ihe.gazelle.saml.assertion.AssertionType aClass) {
        return (((((!(aClass.getSubject() == null) && !(aClass.getSubject().getNameID() == null)) && !(((String) aClass.getSubject().getNameID().getNameQualifier()) == null)) && !(((String) aClass.getSubject().getNameID().getValue()) == null)) && aClass.getSubject().getNameID().getNameQualifier().equals("urn:gs1:gln")) && ((Object) aClass.getSubject().getNameID().getValue().length()).equals(new Integer(13)));

    }

    /**
     * Validation of instance by a constraint : ch_xua_032_Organization
     * In the Heathcare Professional Extension, the Assistant Extension and the Technical User Extension, The organization attribute ( urn:oasis:names:tc:xspa:1.0:subject:organization ) of the <AttributeStatement> MUST convey the name of the organizations or groups the subject is a member of.
     */
    private static boolean _validateCHXUA_AssertionRoleHCP_Ch_xua_032_Organization(net.ihe.gazelle.saml.assertion.AssertionType aClass) {
        Boolean result1;
        result1 = true;

        /* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
        try {
            for (AttributeStatementType anElement1 : aClass.getAttributeStatement()) {
                java.util.ArrayList<AttributeType> result2;
                result2 = new java.util.ArrayList<AttributeType>();

                /* Iterator Select: Select all elements which fulfill the condition. */
                try {
                    for (AttributeType anElement2 : anElement1.getAttribute()) {
                        Boolean result3;
                        result3 = true;

                        /* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
                        try {
                            for (AttributeValueType anElement3 : anElement2.getAttributeValue()) {
                                if (!(tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(anElement3.getListStringValues()) > new Integer(0))) {
                                    result3 = false;
                                    break;
                                }
                                // no else
                            }
                        } catch (Exception e) {
                        }

                        if ((((!(((String) anElement2.getName()) == null) && anElement2.getName().equals("urn:oasis:names:tc:xspa:1.0:subject:organization")) && (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(anElement2.getAttributeValue()) > new Integer(0))) && result3)) {
                            result2.add(anElement2);
                        }
                        // no else
                    }
                } catch (Exception e) {
                }

                if (!((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(anElement1.getAttribute()) > new Integer(0)) && ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(new Integer(1)))) {
                    result1 = false;
                    break;
                }
                // no else
            }
        } catch (Exception e) {
        }

        return result1;


    }

    /**
     * Validation of instance by a constraint : ch_xua_033_OrganizationId
     * In the Heathcare Professional Extension, the Assistant Extension and the Technical User Extension, The organization ID attribute ( urn:oasis:names:tc:xspa:1.0:subject:organization-id ) MUST convey the identifiers of the organizations or groups the subject is assigned to. The identifiers MUST be OID in the format of URN as registered in the healthcare provider directory.
     */
    private static boolean _validateCHXUA_AssertionRoleHCP_Ch_xua_033_OrganizationId(net.ihe.gazelle.saml.assertion.AssertionType aClass) {
        Boolean result1;
        result1 = true;

        /* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
        try {
            for (AttributeStatementType anElement1 : aClass.getAttributeStatement()) {
                java.util.ArrayList<AttributeType> result2;
                result2 = new java.util.ArrayList<AttributeType>();

                /* Iterator Select: Select all elements which fulfill the condition. */
                try {
                    for (AttributeType anElement2 : anElement1.getAttribute()) {
                        Boolean result3;
                        result3 = true;

                        /* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
                        try {
                            for (AttributeValueType anElement3 : anElement2.getAttributeValue()) {
                                if (!(tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(anElement3.getListStringValues()) > new Integer(0))) {
                                    result3 = false;
                                    break;
                                }
                                // no else
                            }
                        } catch (Exception e) {
                        }

                        if ((((!(((String) anElement2.getName()) == null) && anElement2.getName().equals("urn:oasis:names:tc:xspa:1.0:subject:organization-id")) && (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(anElement2.getAttributeValue()) > new Integer(0))) && result3)) {
                            result2.add(anElement2);
                        }
                        // no else
                    }
                } catch (Exception e) {
                }

                if (!((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(anElement1.getAttribute()) > new Integer(0)) && ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(new Integer(1)))) {
                    result1 = false;
                    break;
                }
                // no else
            }
        } catch (Exception e) {
        }

        return result1;


    }

    /**
     * Validation of instance by a constraint : ch_xua_034_SubjectConfNameID
     * The <SubjectConfirmation> element MUST contain a <NameID> child element. The <NameID> element must convey the GLN of the assistant with name qualifier name qualifier attribute set to urn:gs1:gln (Assistant Extension) or the unique ID the technical user is registered within the community and NameQualifier  urn:e-health-suisse:technical-user-id  (Technical User Extension)
     */
    private static boolean _validateCHXUA_AssertionRoleHCP_Ch_xua_034_SubjectConfNameID(net.ihe.gazelle.saml.assertion.AssertionType aClass) {
        Boolean result1;
        result1 = true;

        /* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
        try {
            for (SubjectConfirmationType anElement1 : aClass.getSubject().getSubjectConfirmation()) {
                if (!(!(!(anElement1.getNameID() == null) && !(((String) anElement1.getNameID().getNameQualifier()) == null)) || (anElement1.getNameID().getNameQualifier().equals("urn:e-health-suisse:technical-user-id") || anElement1.getNameID().getNameQualifier().equals("urn:gs1:gln")))) {
                    result1 = false;
                    break;
                }
                // no else
            }
        } catch (Exception e) {
        }

        return (!(aClass.getSubject() == null) && result1);


    }

    /**
     * Validation of instance by a constraint : ch_xua_098_PurposeOfUse
     * The purpose of use attribute ( urn:oasis:names:tc:xspa:1.0:subject:purposeofuse ) of the <AttributeStatement> MUST be either code NORM or EMER from code system 2.16.756.5.30.1.127.3.10.5 of the CH:EPR value set.
     */
    private static boolean _validateCHXUA_AssertionRoleHCP_Ch_xua_098_PurposeOfUse(net.ihe.gazelle.saml.assertion.AssertionType aClass) {
        Boolean result1;
        result1 = true;

        /* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
        try {
            for (SubjectConfirmationType anElement1 : aClass.getSubject().getSubjectConfirmation()) {
                if (!(((anElement1.getNameID() == null) || (((String) anElement1.getNameID().getNameQualifier()) == null)) || !anElement1.getNameID().getNameQualifier().equals("urn:e-health-suisse:technical-user-id"))) {
                    result1 = false;
                    break;
                }
                // no else
            }
        } catch (Exception e) {
        }

        Boolean result2;
        result2 = true;

        /* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
        try {
            for (AttributeStatementType anElement2 : aClass.getAttributeStatement()) {
                Boolean result3;
                result3 = false;

                /* Iterator Exists: Iterate and check, if any element fulfills the condition. */
                try {
                    for (AttributeType anElement3 : anElement2.getAttribute()) {
                        Boolean result4;
                        result4 = true;

                        /* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
                        try {
                            for (AttributeValueType anElement4 : anElement3.getAttributeValue()) {
                                if (!((!(anElement4.getPurposeOfUse() == null) && !(((String) anElement4.getPurposeOfUse().getCode()) == null)) && (anElement4.getPurposeOfUse().getCode().equals("NORM") || anElement4.getPurposeOfUse().getCode().equals("EMER")))) {
                                    result4 = false;
                                    break;
                                }
                                // no else
                            }
                        } catch (Exception e) {
                        }

                        if ((((!(((String) anElement3.getName()) == null) && anElement3.getName().equals("urn:oasis:names:tc:xspa:1.0:subject:purposeofuse")) && (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(anElement3.getAttributeValue()) > new Integer(0))) && result4)) {
                            result3 = true;
                            break;
                        }
                        // no else
                    }
                } catch (Exception e) {
                }

                if (!((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(anElement2.getAttribute()) > new Integer(0)) && result3)) {
                    result2 = false;
                    break;
                }
                // no else
            }
        } catch (Exception e) {
        }

        return (!result1 || ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getAttributeStatement()) > new Integer(0)) && result2));


    }

    /**
     * Validation of template-constraint by a constraint : CHXUA_AssertionRoleHCP
     * Verify if an element can be token as a Template of type CHXUA_AssertionRoleHCP
     */
    private static boolean _isCHXUA_AssertionRoleHCPTemplate(net.ihe.gazelle.saml.assertion.AssertionType aClass) {
        Boolean result1;
        result1 = true;

        /* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
        try {
            for (AttributeStatementType anElement1 : aClass.getAttributeStatement()) {
                Boolean result2;
                result2 = false;

                /* Iterator Exists: Iterate and check, if any element fulfills the condition. */
                try {
                    for (AttributeType anElement2 : anElement1.getAttribute()) {
                        Boolean result3;
                        result3 = true;

                        /* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
                        try {
                            for (AttributeValueType anElement3 : anElement2.getAttributeValue()) {
                                if (!((!(anElement3.getRole() == null) && !(((String) anElement3.getRole().getCode()) == null)) && ((anElement3.getRole().getCode().equals("HCP") || anElement3.getRole().equals("ASS")) || anElement3.getRole().equals("TCU")))) {
                                    result3 = false;
                                    break;
                                }
                                // no else
                            }
                        } catch (Exception e) {
                        }

                        if ((((!(((String) anElement2.getName()) == null) && anElement2.getName().equals("urn:oasis:names:tc:xacml:2.0:subject:role")) && (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(anElement2.getAttributeValue()) > new Integer(0))) && result3)) {
                            result2 = true;
                            break;
                        }
                        // no else
                    }
                } catch (Exception e) {
                }

                if (!((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(anElement1.getAttribute()) > new Integer(0)) && result2)) {
                    result1 = false;
                    break;
                }
                // no else
            }
        } catch (Exception e) {
        }

        return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getAttributeStatement()) > new Integer(0)) && result1);


    }

    /**
     * Validation of class-constraint : CHXUA_AssertionRoleHCP
     * Verify if an element of type CHXUA_AssertionRoleHCP can be validated by CHXUA_AssertionRoleHCP
     */
    public static boolean _isCHXUA_AssertionRoleHCP(net.ihe.gazelle.saml.assertion.AssertionType aClass) {
        return _isCHXUA_AssertionRoleHCPTemplate(aClass);
    }

    /**
     * Validate as a template
     * name ::   CHXUA_AssertionRoleHCP
     * class ::  net.ihe.gazelle.saml.assertion.AssertionType
     */
    public static void _validateCHXUA_AssertionRoleHCP(net.ihe.gazelle.saml.assertion.AssertionType aClass, String location, List<Notification> diagnostic) {
        if (_isCHXUA_AssertionRoleHCP(aClass)) {
            executeCons_CHXUA_AssertionRoleHCP_Ch_xua_031_SubjetNameID(aClass, location, diagnostic);
            executeCons_CHXUA_AssertionRoleHCP_Ch_xua_032_Organization(aClass, location, diagnostic);
            executeCons_CHXUA_AssertionRoleHCP_Ch_xua_033_OrganizationId(aClass, location, diagnostic);
            executeCons_CHXUA_AssertionRoleHCP_Ch_xua_034_SubjectConfNameID(aClass, location, diagnostic);
            executeCons_CHXUA_AssertionRoleHCP_Ch_xua_098_PurposeOfUse(aClass, location, diagnostic);
        }
    }

    private static void executeCons_CHXUA_AssertionRoleHCP_Ch_xua_031_SubjetNameID(net.ihe.gazelle.saml.assertion.AssertionType aClass,
                                                                                   String location, List<Notification> diagnostic) {
        Notification notif = null;
        try {
            if (!(_validateCHXUA_AssertionRoleHCP_Ch_xua_031_SubjetNameID(aClass))) {
                notif = new Error();
            } else {
                notif = new Note();
            }
        } catch (Exception e) {
            notif = new Error();
        }
        notif.setTest("ch_xua_031_SubjetNameID");
        notif.setDescription("In the Heathcare Professional Extension, the Assistant Extension and the Technical User Extension, The <NameID> child element of the <Subject> element MUST contain the GLN of the subject (responsible healthcare professional) with name qualifier attribute set to urn:gs1:gln.");
        notif.setLocation(location);
        notif.setIdentifiant("chxua-CHXUA_AssertionRoleHCP-ch_xua_031_SubjetNameID");
        notif.getAssertions().add(new Assertion("CH-XUA", "CH-XUA-031"));
        diagnostic.add(notif);
    }

    private static void executeCons_CHXUA_AssertionRoleHCP_Ch_xua_032_Organization(net.ihe.gazelle.saml.assertion.AssertionType aClass,
                                                                                   String location, List<Notification> diagnostic) {
        Notification notif = null;
        try {
            if (!(_validateCHXUA_AssertionRoleHCP_Ch_xua_032_Organization(aClass))) {
                notif = new Error();
            } else {
                notif = new Note();
            }
        } catch (Exception e) {
            notif = new Error();
        }
        notif.setTest("ch_xua_032_Organization");
        notif.setDescription("In the Heathcare Professional Extension, the Assistant Extension and the Technical User Extension, The organization attribute ( urn:oasis:names:tc:xspa:1.0:subject:organization ) of the <AttributeStatement> MUST convey the name of the organizations or groups the subject is a member of.");
        notif.setLocation(location);
        notif.setIdentifiant("chxua-CHXUA_AssertionRoleHCP-ch_xua_032_Organization");
        notif.getAssertions().add(new Assertion("CH-XUA", "CH-XUA-032"));
        diagnostic.add(notif);
    }

    private static void executeCons_CHXUA_AssertionRoleHCP_Ch_xua_033_OrganizationId(net.ihe.gazelle.saml.assertion.AssertionType aClass,
                                                                                     String location, List<Notification> diagnostic) {
        Notification notif = null;
        try {
            if (!(_validateCHXUA_AssertionRoleHCP_Ch_xua_033_OrganizationId(aClass))) {
                notif = new Error();
            } else {
                notif = new Note();
            }
        } catch (Exception e) {
            notif = new Error();
        }
        notif.setTest("ch_xua_033_OrganizationId");
        notif.setDescription("In the Heathcare Professional Extension, the Assistant Extension and the Technical User Extension, The organization ID attribute ( urn:oasis:names:tc:xspa:1.0:subject:organization-id ) MUST convey the identifiers of the organizations or groups the subject is assigned to. The identifiers MUST be OID in the format of URN as registered in the healthcare provider directory.");
        notif.setLocation(location);
        notif.setIdentifiant("chxua-CHXUA_AssertionRoleHCP-ch_xua_033_OrganizationId");
        notif.getAssertions().add(new Assertion("CH-XUA", "CH-XUA-033"));
        diagnostic.add(notif);
    }

    private static void executeCons_CHXUA_AssertionRoleHCP_Ch_xua_034_SubjectConfNameID(net.ihe.gazelle.saml.assertion.AssertionType aClass,
                                                                                        String location, List<Notification> diagnostic) {
        Notification notif = null;
        try {
            if (!(_validateCHXUA_AssertionRoleHCP_Ch_xua_034_SubjectConfNameID(aClass))) {
                notif = new Error();
            } else {
                notif = new Note();
            }
        } catch (Exception e) {
            notif = new Error();
        }
        notif.setTest("ch_xua_034_SubjectConfNameID");
        notif.setDescription("The <SubjectConfirmation> element MUST contain a <NameID> child element. The <NameID> element must convey the GLN of the assistant with name qualifier name qualifier attribute set to urn:gs1:gln (Assistant Extension) or the unique ID the technical user is registered within the community and NameQualifier  urn:e-health-suisse:technical-user-id  (Technical User Extension)");
        notif.setLocation(location);
        notif.setIdentifiant("chxua-CHXUA_AssertionRoleHCP-ch_xua_034_SubjectConfNameID");
        notif.getAssertions().add(new Assertion("CH-XUA", "CH-XUA-034"));
        diagnostic.add(notif);
    }

    private static void executeCons_CHXUA_AssertionRoleHCP_Ch_xua_098_PurposeOfUse(net.ihe.gazelle.saml.assertion.AssertionType aClass,
                                                                                   String location, List<Notification> diagnostic) {
        Notification notif = null;
        try {
            if (!(_validateCHXUA_AssertionRoleHCP_Ch_xua_098_PurposeOfUse(aClass))) {
                notif = new Error();
            } else {
                notif = new Note();
            }
        } catch (Exception e) {
            notif = new Error();
        }
        notif.setTest("ch_xua_098_PurposeOfUse");
        notif.setDescription("The purpose of use attribute ( urn:oasis:names:tc:xspa:1.0:subject:purposeofuse ) of the <AttributeStatement> MUST be either code NORM or EMER from code system 2.16.756.5.30.1.127.3.10.5 of the CH:EPR value set.");
        notif.setLocation(location);
        notif.setIdentifiant("chxua-CHXUA_AssertionRoleHCP-ch_xua_098_PurposeOfUse");
        notif.getAssertions().add(new Assertion("CH-XUA", "CH-XUA-098"));
        diagnostic.add(notif);
    }

}
