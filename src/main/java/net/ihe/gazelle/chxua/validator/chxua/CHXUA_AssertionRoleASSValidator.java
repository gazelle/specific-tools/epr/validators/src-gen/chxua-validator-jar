package net.ihe.gazelle.chxua.validator.chxua;

import net.ihe.gazelle.saml.assertion.*;
import net.ihe.gazelle.saml.delegation.DelegateType;
import net.ihe.gazelle.saml.delegation.DelegationRestrictionType;
import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import java.util.List;


/**
 * class :        CHXUA_AssertionRoleASS
 * package :   chxua
 * Template Class
 * Template identifier :
 * Class of test : AssertionSpec
 */
public final class CHXUA_AssertionRoleASSValidator {


    private CHXUA_AssertionRoleASSValidator() {
    }


    /**
     * Validation of instance by a constraint : ch_xua_035_AudienceRestriction
     * The <Conditions> element MUST contain a <AudienceRestriction> element coveying a single <Audience> child element with the value set to  urn:e-health-suisse:token-audience:all-communities .
     */
    private static boolean _validateCHXUA_AssertionRoleASS_Ch_xua_035_AudienceRestriction(net.ihe.gazelle.saml.assertion.AssertionType aClass) {
        java.util.ArrayList<String> result1;
        result1 = new java.util.ArrayList<String>();

        /* Iterator Collect: Iterate through all elements and collect them. Elements which are collections are flattened. */
        for (AudienceRestrictionType anElement1 : aClass.getConditions().getAudienceRestriction()) {
            result1.addAll(anElement1.getAudience());
        }

        java.util.ArrayList<String> result3;
        result3 = new java.util.ArrayList<String>();

        /* Iterator Collect: Iterate through all elements and collect them. Elements which are collections are flattened. */
        for (AudienceRestrictionType anElement2 : aClass.getConditions().getAudienceRestriction()) {
            result3.addAll(anElement2.getAudience());
        }
        Boolean result2;
        result2 = true;

        /* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
        try {
            for (String anElement3 : result3) {
                if (!anElement3.equals("urn:e-health-suisse:token-audience:all-communities")) {
                    result2 = false;
                    break;
                }
                // no else
            }
        } catch (Exception e) {
        }

        return ((!(aClass.getConditions() == null) && ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(new Integer(1))) && result2);


    }

    /**
     * Validation of instance by a constraint : ch_xua_088_Delegation
     * The <Conditions> element MUST contain a single <Condition> element with a <NameID> child element with Format  urn:oasis:names:tc:SAML:2.0:nameid-format:persistent  and NameQualifier  urn:gs1:gln  conveying the GLN of the assistant.
     */
    private static boolean _validateCHXUA_AssertionRoleASS_Ch_xua_088_Delegation(net.ihe.gazelle.saml.assertion.AssertionType aClass) {
        java.util.ArrayList<DelegationRestrictionType> result1;
        result1 = new java.util.ArrayList<DelegationRestrictionType>();

        /* Iterator Collect: Iterate through all elements and collect them. Elements which are collections are flattened. */
        for (ConditionAbstractType anElement1 : aClass.getConditions().getCondition()) {
            result1.add(((DelegationRestrictionType) anElement1));
        }

        java.util.ArrayList<DelegationRestrictionType> result4;
        result4 = new java.util.ArrayList<DelegationRestrictionType>();

        /* Iterator Collect: Iterate through all elements and collect them. Elements which are collections are flattened. */
        for (ConditionAbstractType anElement2 : aClass.getConditions().getCondition()) {
            result4.add(((DelegationRestrictionType) anElement2));
        }
        java.util.ArrayList<DelegateType> result3;
        result3 = new java.util.ArrayList<DelegateType>();

        /* Iterator Collect: Iterate through all elements and collect them. Elements which are collections are flattened. */
        for (DelegationRestrictionType anElement3 : result4) {
            result3.addAll(anElement3.getDelegate());
        }
        Boolean result2;
        result2 = true;

        /* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
        try {
            for (DelegateType anElement4 : result3) {
                if (!((!(anElement4.getNameID() == null) && anElement4.getNameID().getFormat().equals("urn:oasis:names:tc:SAML:2.0:nameid-format:persistent")) && anElement4.getNameID().getNameQualifier().equals("urn:gs1:gln"))) {
                    result2 = false;
                    break;
                }
                // no else
            }
        } catch (Exception e) {
        }

        return ((!(aClass.getConditions() == null) && ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(new Integer(1))) && result2);


    }

    /**
     * Validation of instance by a constraint : ch_xua_097_PurposeOfUse
     * The purpose of use attribute ( urn:oasis:names:tc:xspa:1.0:subject:purposeofuse ) attribute MUST be code NORM from code system 2.16.756.5.30.1.127.3.10.5 of the CH:EPR value set.
     */
    private static boolean _validateCHXUA_AssertionRoleASS_Ch_xua_097_PurposeOfUse(net.ihe.gazelle.saml.assertion.AssertionType aClass) {
        Boolean result1;
        result1 = true;

        /* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
        try {
            for (AttributeStatementType anElement1 : aClass.getAttributeStatement()) {
                Boolean result2;
                result2 = false;

                /* Iterator Exists: Iterate and check, if any element fulfills the condition. */
                try {
                    for (AttributeType anElement2 : anElement1.getAttribute()) {
                        Boolean result3;
                        result3 = true;

                        /* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
                        try {
                            for (AttributeValueType anElement3 : anElement2.getAttributeValue()) {
                                if (!((!(anElement3.getPurposeOfUse() == null) && !(((String) anElement3.getPurposeOfUse().getCode()) == null)) && anElement3.getPurposeOfUse().getCode().equals("NORM"))) {
                                    result3 = false;
                                    break;
                                }
                                // no else
                            }
                        } catch (Exception e) {
                        }

                        if ((((!(((String) anElement2.getName()) == null) && anElement2.getName().equals("urn:oasis:names:tc:xspa:1.0:subject:purposeofuse")) && (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(anElement2.getAttributeValue()) > new Integer(0))) && result3)) {
                            result2 = true;
                            break;
                        }
                        // no else
                    }
                } catch (Exception e) {
                }

                if (!((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(anElement1.getAttribute()) > new Integer(0)) && result2)) {
                    result1 = false;
                    break;
                }
                // no else
            }
        } catch (Exception e) {
        }

        return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getAttributeStatement()) > new Integer(0)) && result1);


    }

    /**
     * Validation of template-constraint by a constraint : CHXUA_AssertionRoleASS
     * Verify if an element can be token as a Template of type CHXUA_AssertionRoleASS
     */
    private static boolean _isCHXUA_AssertionRoleASSTemplate(net.ihe.gazelle.saml.assertion.AssertionType aClass) {
        Boolean result1;
        result1 = true;

        /* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
        try {
            for (SubjectConfirmationType anElement1 : aClass.getSubject().getSubjectConfirmation()) {
                if (!((!(anElement1.getNameID() == null) && !(((String) anElement1.getNameID().getNameQualifier()) == null)) && anElement1.getNameID().getNameQualifier().equals("urn:gs1:gln"))) {
                    result1 = false;
                    break;
                }
                // no else
            }
        } catch (Exception e) {
        }

        return (!(aClass.getSubject() == null) && result1);


    }

    /**
     * Validation of class-constraint : CHXUA_AssertionRoleASS
     * Verify if an element of type CHXUA_AssertionRoleASS can be validated by CHXUA_AssertionRoleASS
     */
    public static boolean _isCHXUA_AssertionRoleASS(net.ihe.gazelle.saml.assertion.AssertionType aClass) {
        return _isCHXUA_AssertionRoleASSTemplate(aClass);
    }

    /**
     * Validate as a template
     * name ::   CHXUA_AssertionRoleASS
     * class ::  net.ihe.gazelle.saml.assertion.AssertionType
     */
    public static void _validateCHXUA_AssertionRoleASS(net.ihe.gazelle.saml.assertion.AssertionType aClass, String location, List<Notification> diagnostic) {
        if (_isCHXUA_AssertionRoleASS(aClass)) {
            executeCons_CHXUA_AssertionRoleASS_Ch_xua_035_AudienceRestriction(aClass, location, diagnostic);
            executeCons_CHXUA_AssertionRoleASS_Ch_xua_088_Delegation(aClass, location, diagnostic);
            executeCons_CHXUA_AssertionRoleASS_Ch_xua_097_PurposeOfUse(aClass, location, diagnostic);
        }
    }

    private static void executeCons_CHXUA_AssertionRoleASS_Ch_xua_035_AudienceRestriction(net.ihe.gazelle.saml.assertion.AssertionType aClass,
                                                                                          String location, List<Notification> diagnostic) {
        Notification notif = null;
        try {
            if (!(_validateCHXUA_AssertionRoleASS_Ch_xua_035_AudienceRestriction(aClass))) {
                notif = new Error();
            } else {
                notif = new Note();
            }
        } catch (Exception e) {
            notif = new Error();
        }
        notif.setTest("ch_xua_035_AudienceRestriction");
        notif.setDescription("The <Conditions> element MUST contain a <AudienceRestriction> element coveying a single <Audience> child element with the value set to  urn:e-health-suisse:token-audience:all-communities .");
        notif.setLocation(location);
        notif.setIdentifiant("chxua-CHXUA_AssertionRoleASS-ch_xua_035_AudienceRestriction");
        notif.getAssertions().add(new Assertion("CH-XUA", "CH-XUA-035"));
        diagnostic.add(notif);
    }

    private static void executeCons_CHXUA_AssertionRoleASS_Ch_xua_088_Delegation(net.ihe.gazelle.saml.assertion.AssertionType aClass,
                                                                                 String location, List<Notification> diagnostic) {
        Notification notif = null;
        try {
            if (!(_validateCHXUA_AssertionRoleASS_Ch_xua_088_Delegation(aClass))) {
                notif = new Error();
            } else {
                notif = new Note();
            }
        } catch (Exception e) {
            notif = new Error();
        }
        notif.setTest("ch_xua_088_Delegation");
        notif.setDescription("The <Conditions> element MUST contain a single <Condition> element with a <NameID> child element with Format  urn:oasis:names:tc:SAML:2.0:nameid-format:persistent  and NameQualifier  urn:gs1:gln  conveying the GLN of the assistant.");
        notif.setLocation(location);
        notif.setIdentifiant("chxua-CHXUA_AssertionRoleASS-ch_xua_088_Delegation");
        notif.getAssertions().add(new Assertion("CH-XUA", "CH-XUA-088"));
        diagnostic.add(notif);
    }

    private static void executeCons_CHXUA_AssertionRoleASS_Ch_xua_097_PurposeOfUse(net.ihe.gazelle.saml.assertion.AssertionType aClass,
                                                                                   String location, List<Notification> diagnostic) {
        Notification notif = null;
        try {
            if (!(_validateCHXUA_AssertionRoleASS_Ch_xua_097_PurposeOfUse(aClass))) {
                notif = new Error();
            } else {
                notif = new Note();
            }
        } catch (Exception e) {
            notif = new Error();
        }
        notif.setTest("ch_xua_097_PurposeOfUse");
        notif.setDescription("The purpose of use attribute ( urn:oasis:names:tc:xspa:1.0:subject:purposeofuse ) attribute MUST be code NORM from code system 2.16.756.5.30.1.127.3.10.5 of the CH:EPR value set.");
        notif.setLocation(location);
        notif.setIdentifiant("chxua-CHXUA_AssertionRoleASS-ch_xua_097_PurposeOfUse");
        notif.getAssertions().add(new Assertion("CH-XUA", "CH-XUA-092"));
        diagnostic.add(notif);
    }

}
