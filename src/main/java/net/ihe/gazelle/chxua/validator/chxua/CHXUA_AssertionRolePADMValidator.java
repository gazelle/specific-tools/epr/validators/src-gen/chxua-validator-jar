package net.ihe.gazelle.chxua.validator.chxua;

import net.ihe.gazelle.saml.assertion.AttributeStatementType;
import net.ihe.gazelle.saml.assertion.AttributeType;
import net.ihe.gazelle.saml.assertion.AttributeValueType;
import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import java.util.List;


/**
 * class :        CHXUA_AssertionRolePADM
 * package :   chxua
 * Template Class
 * Template identifier :
 * Class of test : AssertionSpec
 */
public final class CHXUA_AssertionRolePADMValidator {


    private CHXUA_AssertionRolePADMValidator() {
    }


    /**
     * Validation of instance by a constraint : ch_xua_036_NameID
     * The <NameID> child element of the <Subject> element MUST contain the unique ID the administrator is registered with in the community and the name qualifier attribute set to  urn:e-health-suisse:policy-administrator-id .
     */
    private static boolean _validateCHXUA_AssertionRolePADM_Ch_xua_036_NameID(net.ihe.gazelle.saml.assertion.AssertionType aClass) {
        return ((((!(aClass.getSubject() == null) && !(aClass.getSubject().getNameID() == null)) && !(((String) aClass.getSubject().getNameID().getNameQualifier()) == null)) && !(((String) aClass.getSubject().getNameID().getValue()) == null)) && aClass.getSubject().getNameID().getNameQualifier().equals("urn:e-health-suisse:policy-administrator-id"));

    }

    /**
     * Validation of instance by a constraint : ch_xua_094_PurposeOfUse
     * The purpose of use attribute ( urn:oasis:names:tc:xspa:1.0:subject:purposeofuse ) attribute MUST be code NORM from code system 2.16.756.5.30.1.127.3.10.5 of the CH:EPR value set.
     */
    private static boolean _validateCHXUA_AssertionRolePADM_Ch_xua_094_PurposeOfUse(net.ihe.gazelle.saml.assertion.AssertionType aClass) {
        Boolean result1;
        result1 = true;

        /* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
        try {
            for (AttributeStatementType anElement1 : aClass.getAttributeStatement()) {
                Boolean result2;
                result2 = false;

                /* Iterator Exists: Iterate and check, if any element fulfills the condition. */
                try {
                    for (AttributeType anElement2 : anElement1.getAttribute()) {
                        Boolean result3;
                        result3 = true;

                        /* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
                        try {
                            for (AttributeValueType anElement3 : anElement2.getAttributeValue()) {
                                if (!((!(anElement3.getPurposeOfUse() == null) && !(((String) anElement3.getPurposeOfUse().getCode()) == null)) && anElement3.getPurposeOfUse().getCode().equals("NORM"))) {
                                    result3 = false;
                                    break;
                                }
                                // no else
                            }
                        } catch (Exception e) {
                        }

                        if ((((!(((String) anElement2.getName()) == null) && anElement2.getName().equals("urn:oasis:names:tc:xspa:1.0:subject:purposeofuse")) && (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(anElement2.getAttributeValue()) > new Integer(0))) && result3)) {
                            result2 = true;
                            break;
                        }
                        // no else
                    }
                } catch (Exception e) {
                }

                if (!((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(anElement1.getAttribute()) > new Integer(0)) && result2)) {
                    result1 = false;
                    break;
                }
                // no else
            }
        } catch (Exception e) {
        }

        return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getAttributeStatement()) > new Integer(0)) && result1);


    }

    /**
     * Validation of template-constraint by a constraint : CHXUA_AssertionRolePADM
     * Verify if an element can be token as a Template of type CHXUA_AssertionRolePADM
     */
    private static boolean _isCHXUA_AssertionRolePADMTemplate(net.ihe.gazelle.saml.assertion.AssertionType aClass) {
        Boolean result1;
        result1 = true;

        /* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
        try {
            for (AttributeStatementType anElement1 : aClass.getAttributeStatement()) {
                Boolean result2;
                result2 = false;

                /* Iterator Exists: Iterate and check, if any element fulfills the condition. */
                try {
                    for (AttributeType anElement2 : anElement1.getAttribute()) {
                        Boolean result3;
                        result3 = true;

                        /* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
                        try {
                            for (AttributeValueType anElement3 : anElement2.getAttributeValue()) {
                                if (!((!(anElement3.getRole() == null) && !(((String) anElement3.getRole().getCode()) == null)) && anElement3.getRole().getCode().equals("PADM"))) {
                                    result3 = false;
                                    break;
                                }
                                // no else
                            }
                        } catch (Exception e) {
                        }

                        if ((((!(((String) anElement2.getName()) == null) && anElement2.getName().equals("urn:oasis:names:tc:xacml:2.0:subject:role")) && (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(anElement2.getAttributeValue()) > new Integer(0))) && result3)) {
                            result2 = true;
                            break;
                        }
                        // no else
                    }
                } catch (Exception e) {
                }

                if (!((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(anElement1.getAttribute()) > new Integer(0)) && result2)) {
                    result1 = false;
                    break;
                }
                // no else
            }
        } catch (Exception e) {
        }

        return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getAttributeStatement()) > new Integer(0)) && result1);


    }

    /**
     * Validation of class-constraint : CHXUA_AssertionRolePADM
     * Verify if an element of type CHXUA_AssertionRolePADM can be validated by CHXUA_AssertionRolePADM
     */
    public static boolean _isCHXUA_AssertionRolePADM(net.ihe.gazelle.saml.assertion.AssertionType aClass) {
        return _isCHXUA_AssertionRolePADMTemplate(aClass);
    }

    /**
     * Validate as a template
     * name ::   CHXUA_AssertionRolePADM
     * class ::  net.ihe.gazelle.saml.assertion.AssertionType
     */
    public static void _validateCHXUA_AssertionRolePADM(net.ihe.gazelle.saml.assertion.AssertionType aClass, String location, List<Notification> diagnostic) {
        if (_isCHXUA_AssertionRolePADM(aClass)) {
            executeCons_CHXUA_AssertionRolePADM_Ch_xua_036_NameID(aClass, location, diagnostic);
            executeCons_CHXUA_AssertionRolePADM_Ch_xua_094_PurposeOfUse(aClass, location, diagnostic);
        }
    }

    private static void executeCons_CHXUA_AssertionRolePADM_Ch_xua_036_NameID(net.ihe.gazelle.saml.assertion.AssertionType aClass,
                                                                              String location, List<Notification> diagnostic) {
        Notification notif = null;
        try {
            if (!(_validateCHXUA_AssertionRolePADM_Ch_xua_036_NameID(aClass))) {
                notif = new Error();
            } else {
                notif = new Note();
            }
        } catch (Exception e) {
            notif = new Error();
        }
        notif.setTest("ch_xua_036_NameID");
        notif.setDescription("The <NameID> child element of the <Subject> element MUST contain the unique ID the administrator is registered with in the community and the name qualifier attribute set to  urn:e-health-suisse:policy-administrator-id .");
        notif.setLocation(location);
        notif.setIdentifiant("chxua-CHXUA_AssertionRolePADM-ch_xua_036_NameID");
        notif.getAssertions().add(new Assertion("CH-XUA", "CH-XUA-036"));
        diagnostic.add(notif);
    }

    private static void executeCons_CHXUA_AssertionRolePADM_Ch_xua_094_PurposeOfUse(net.ihe.gazelle.saml.assertion.AssertionType aClass,
                                                                                    String location, List<Notification> diagnostic) {
        Notification notif = null;
        try {
            if (!(_validateCHXUA_AssertionRolePADM_Ch_xua_094_PurposeOfUse(aClass))) {
                notif = new Error();
            } else {
                notif = new Note();
            }
        } catch (Exception e) {
            notif = new Error();
        }
        notif.setTest("ch_xua_094_PurposeOfUse");
        notif.setDescription("The purpose of use attribute ( urn:oasis:names:tc:xspa:1.0:subject:purposeofuse ) attribute MUST be code NORM from code system 2.16.756.5.30.1.127.3.10.5 of the CH:EPR value set.");
        notif.setLocation(location);
        notif.setIdentifiant("chxua-CHXUA_AssertionRolePADM-ch_xua_094_PurposeOfUse");
        notif.getAssertions().add(new Assertion("CH-XUA", "CH-XUA-094"));
        diagnostic.add(notif);
    }

}
