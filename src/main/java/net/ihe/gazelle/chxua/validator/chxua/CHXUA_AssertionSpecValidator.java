package net.ihe.gazelle.chxua.validator.chxua;

import net.ihe.gazelle.saml.assertion.AttributeStatementType;
import net.ihe.gazelle.saml.assertion.AttributeType;
import net.ihe.gazelle.saml.assertion.AttributeValueType;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.*;

import java.util.List;


/**
 * class :        CHXUA_AssertionSpec
 * package :   chxua
 * Constraint Spec Class
 * class of test : AssertionSpec
 */
public final class CHXUA_AssertionSpecValidator {


    private CHXUA_AssertionSpecValidator() {
    }


    /**
     * Validation of instance by a constraint : ch_xua_002_SubjectID
     * There MUST be one <Attribute> element with the name attribute  urn:oasis:names:tc:xspa:1.0:subject:subject-id . The <AttributeValue> child element MUST convey the subject’s real world name as plain text as defined by IHE XUA in all extensions.
     */
    private static boolean _validateCHXUA_AssertionSpec_Ch_xua_002_SubjectID(net.ihe.gazelle.saml.assertion.AssertionType aClass) {
        Boolean result1;
        result1 = true;

        /* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
        try {
            for (AttributeStatementType anElement1 : aClass.getAttributeStatement()) {
                java.util.ArrayList<AttributeType> result2;
                result2 = new java.util.ArrayList<AttributeType>();

                /* Iterator Select: Select all elements which fulfill the condition. */
                try {
                    for (AttributeType anElement2 : anElement1.getAttribute()) {
                        if (((!(((String) anElement2.getName()) == null) && anElement2.getName().equals("urn:oasis:names:tc:xspa:1.0:subject:subject-id")) && ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(anElement2.getAttributeValue())).equals(new Integer(1)))) {
                            result2.add(anElement2);
                        }
                        // no else
                    }
                } catch (Exception e) {
                }

                if (!((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(anElement1.getAttribute()) > new Integer(0)) && ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(new Integer(1)))) {
                    result1 = false;
                    break;
                }
                // no else
            }
        } catch (Exception e) {
        }

        return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getAttributeStatement()) > new Integer(0)) && result1);


    }

    /**
     * Validation of instance by a constraint : ch_xua_005_Role
     * The User Assertion MUST contain the attribute: /AttributeStatement/Attribute[@name=”urn:oasis:names:tc:xacml:2.0:subject:role”]. The AttributeValue MUST be PAT, HCP, DADM, PADM or REP for Representative
     */
    private static boolean _validateCHXUA_AssertionSpec_Ch_xua_005_Role(net.ihe.gazelle.saml.assertion.AssertionType aClass) {
        Boolean result1;
        result1 = true;

        /* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
        try {
            for (AttributeStatementType anElement1 : aClass.getAttributeStatement()) {
                java.util.ArrayList<AttributeType> result2;
                result2 = new java.util.ArrayList<AttributeType>();

                /* Iterator Select: Select all elements which fulfill the condition. */
                try {
                    for (AttributeType anElement2 : anElement1.getAttribute()) {
                        Boolean result3;
                        result3 = true;

                        /* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
                        try {
                            for (AttributeValueType anElement3 : anElement2.getAttributeValue()) {
                                if (!((((((!(anElement3.getRole() == null) && !(((String) anElement3.getRole().getCode()) == null)) && anElement3.getRole().getCode().equals("PAT")) || anElement3.getRole().getCode().equals("HCP")) || anElement3.getRole().getCode().equals("DADM")) || anElement3.getRole().getCode().equals("PADM")) || anElement3.getRole().getCode().equals("REP"))) {
                                    result3 = false;
                                    break;
                                }
                                // no else
                            }
                        } catch (Exception e) {
                        }

                        if ((((!(((String) anElement2.getName()) == null) && anElement2.getName().equals("urn:oasis:names:tc:xacml:2.0:subject:role")) && ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(anElement2.getAttributeValue())).equals(new Integer(1))) && result3)) {
                            result2.add(anElement2);
                        }
                        // no else
                    }
                } catch (Exception e) {
                }

                if (!((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(anElement1.getAttribute()) > new Integer(0)) && ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(new Integer(1)))) {
                    result1 = false;
                    break;
                }
                // no else
            }
        } catch (Exception e) {
        }

        return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getAttributeStatement()) > new Integer(0)) && result1);


    }

    /**
     * Validation of instance by a constraint : ch_xua_006_ResourceID
     * The User Assertion MUST contain the attribute: /AttributeStatement/Attribute[@name=”urn:oasis:names:tc:xacml:2.0:resource:resource-id”].
     */
    private static boolean _validateCHXUA_AssertionSpec_Ch_xua_006_ResourceID(net.ihe.gazelle.saml.assertion.AssertionType aClass) {
        Boolean result1;
        result1 = true;

        /* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
        try {
            for (AttributeStatementType anElement1 : aClass.getAttributeStatement()) {
                java.util.ArrayList<AttributeType> result2;
                result2 = new java.util.ArrayList<AttributeType>();

                /* Iterator Select: Select all elements which fulfill the condition. */
                try {
                    for (AttributeType anElement2 : anElement1.getAttribute()) {
                        if (((!(((String) anElement2.getName()) == null) && anElement2.getName().equals("urn:oasis:names:tc:xacml:2.0:resource:resource-id")) && ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(anElement2.getAttributeValue())).equals(new Integer(1)))) {
                            result2.add(anElement2);
                        }
                        // no else
                    }
                } catch (Exception e) {
                }

                if (!((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(anElement1.getAttribute()) > new Integer(0)) && ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(new Integer(1)))) {
                    result1 = false;
                    break;
                }
                // no else
            }
        } catch (Exception e) {
        }

        return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getAttributeStatement()) > new Integer(0)) && result1);


    }

    /**
     * Validation of instance by a constraint : ch_xua_007_PurposeOfUse
     * The User Assertion MUST contain the attribute: /AttributeStatement/Attribute[@name=”urn:oasis:names:tc:xspa:1.0:subject:purposeofuse”].
     */
    private static boolean _validateCHXUA_AssertionSpec_Ch_xua_007_PurposeOfUse(net.ihe.gazelle.saml.assertion.AssertionType aClass) {
        Boolean result1;
        result1 = true;

        /* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
        try {
            for (AttributeStatementType anElement1 : aClass.getAttributeStatement()) {
                java.util.ArrayList<AttributeType> result2;
                result2 = new java.util.ArrayList<AttributeType>();

                /* Iterator Select: Select all elements which fulfill the condition. */
                try {
                    for (AttributeType anElement2 : anElement1.getAttribute()) {
                        if (((!(((String) anElement2.getName()) == null) && anElement2.getName().equals("urn:oasis:names:tc:xspa:1.0:subject:purposeofuse")) && ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(anElement2.getAttributeValue())).equals(new Integer(1)))) {
                            result2.add(anElement2);
                        }
                        // no else
                    }
                } catch (Exception e) {
                }

                if (!((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(anElement1.getAttribute()) > new Integer(0)) && ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(new Integer(1)))) {
                    result1 = false;
                    break;
                }
                // no else
            }
        } catch (Exception e) {
        }

        return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getAttributeStatement()) > new Integer(0)) && result1);


    }

    /**
     * Validation of instance by a constraint : ch_xua_008_ResourceID_EPR_SPID
     * The AttributeValue for ResourceID MUST be the EPR-PID of the patient, to which the transaction refers
     */
    private static boolean _validateCHXUA_AssertionSpec_Ch_xua_008_ResourceID_EPR_SPID(net.ihe.gazelle.saml.assertion.AssertionType aClass) {
        Boolean result1;
        result1 = true;

        /* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
        try {
            for (AttributeStatementType anElement1 : aClass.getAttributeStatement()) {
                java.util.ArrayList<AttributeType> result2;
                result2 = new java.util.ArrayList<AttributeType>();

                /* Iterator Select: Select all elements which fulfill the condition. */
                try {
                    for (AttributeType anElement2 : anElement1.getAttribute()) {
                        Boolean result3;
                        result3 = true;

                        /* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
                        try {
                            for (AttributeValueType anElement3 : anElement2.getAttributeValue()) {
                                Boolean result4;
                                result4 = false;

                                /* Iterator Exists: Iterate and check, if any element fulfills the condition. */
                                try {
                                    for (String anElement4 : anElement3.getListStringValues()) {
                                        if (anElement4.substring(new Integer(1) - 1, new Integer(8)).equals("76133761")) {
                                            result4 = true;
                                            break;
                                        }
                                        // no else
                                    }
                                } catch (Exception e) {
                                }

                                if (!result4) {
                                    result3 = false;
                                    break;
                                }
                                // no else
                            }
                        } catch (Exception e) {
                        }

                        if ((((!(((String) anElement2.getName()) == null) && anElement2.getName().equals("urn:oasis:names:tc:xacml:2.0:resource:resource-id")) && ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(anElement2.getAttributeValue())).equals(new Integer(1))) && result3)) {
                            result2.add(anElement2);
                        }
                        // no else
                    }
                } catch (Exception e) {
                }

                if (!((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(anElement1.getAttribute()) > new Integer(0)) && ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(new Integer(1)))) {
                    result1 = false;
                    break;
                }
                // no else
            }
        } catch (Exception e) {
        }

        return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getAttributeStatement()) > new Integer(0)) && result1);


    }

    /**
     * Validation of instance by a constraint : ch_xua_099_homeCommunityID
     * There MUST be one <Attribute> element with the name attribute: urn:ihe:iti:xca:2010:homeCommunityId . The <AttributeValue> child element MUST convey the value of the Home Community ID (an Object Identifier) assigned to the Community that is initiating the request, using the urn format (that is, “urn:oid:” appended with the OID).
     */
    private static boolean _validateCHXUA_AssertionSpec_Ch_xua_099_homeCommunityID(net.ihe.gazelle.saml.assertion.AssertionType aClass) {
        Boolean result1;
        result1 = true;

        /* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
        try {
            for (AttributeStatementType anElement1 : aClass.getAttributeStatement()) {
                java.util.ArrayList<AttributeType> result2;
                result2 = new java.util.ArrayList<AttributeType>();

                /* Iterator Select: Select all elements which fulfill the condition. */
                try {
                    for (AttributeType anElement2 : anElement1.getAttribute()) {
                        Boolean result3;
                        result3 = true;

                        /* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
                        try {
                            for (AttributeValueType anElement3 : anElement2.getAttributeValue()) {
                                Boolean result4;
                                result4 = false;

                                /* Iterator Exists: Iterate and check, if any element fulfills the condition. */
                                try {
                                    for (String anElement4 : anElement3.getListStringValues()) {
                                        if (anElement4.substring(new Integer(1) - 1, new Integer(8)).equals("urn:oid:")) {
                                            result4 = true;
                                            break;
                                        }
                                        // no else
                                    }
                                } catch (Exception e) {
                                }

                                if (!result4) {
                                    result3 = false;
                                    break;
                                }
                                // no else
                            }
                        } catch (Exception e) {
                        }

                        if ((((!(((String) anElement2.getName()) == null) && anElement2.getName().equals("urn:ihe:iti:xca:2010:homeCommunityId")) && ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(anElement2.getAttributeValue())).equals(new Integer(1))) && result3)) {
                            result2.add(anElement2);
                        }
                        // no else
                    }
                } catch (Exception e) {
                }

                if (!((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(anElement1.getAttribute()) > new Integer(0)) && ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(new Integer(1)))) {
                    result1 = false;
                    break;
                }
                // no else
            }
        } catch (Exception e) {
        }

        return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getAttributeStatement()) > new Integer(0)) && result1);


    }

    /**
     * Validation of class-constraint : CHXUA_AssertionSpec
     * Verify if an element of type CHXUA_AssertionSpec can be validated by CHXUA_AssertionSpec
     */
    public static boolean _isCHXUA_AssertionSpec(net.ihe.gazelle.saml.assertion.AssertionType aClass) {
        return net.ihe.gazelle.samlihe.samlihe.AssertionSpecValidator._isAssertionSpec(aClass);
    }

    /**
     * Validate as a template
     * name ::   CHXUA_AssertionSpec
     * class ::  net.ihe.gazelle.saml.assertion.AssertionType
     */
    public static void _validateCHXUA_AssertionSpec(net.ihe.gazelle.saml.assertion.AssertionType aClass, String location, List<Notification> diagnostic) {
        if (_isCHXUA_AssertionSpec(aClass)) {
            executeCons_CHXUA_AssertionSpec_Ch_xua_002_SubjectID(aClass, location, diagnostic);
            executeCons_CHXUA_AssertionSpec_Ch_xua_005_Role(aClass, location, diagnostic);
            executeCons_CHXUA_AssertionSpec_Ch_xua_006_ResourceID(aClass, location, diagnostic);
            executeCons_CHXUA_AssertionSpec_Ch_xua_007_PurposeOfUse(aClass, location, diagnostic);
            executeCons_CHXUA_AssertionSpec_Ch_xua_008_ResourceID_EPR_SPID(aClass, location, diagnostic);
            executeCons_CHXUA_AssertionSpec_Ch_xua_099_homeCommunityID(aClass, location, diagnostic);
        }
    }

    private static void executeCons_CHXUA_AssertionSpec_Ch_xua_002_SubjectID(net.ihe.gazelle.saml.assertion.AssertionType aClass,
                                                                             String location, List<Notification> diagnostic) {
        Notification notif = null;
        try {
            if (!(_validateCHXUA_AssertionSpec_Ch_xua_002_SubjectID(aClass))) {
                notif = new Error();
            } else {
                notif = new Note();
            }
        } catch (Exception e) {
            notif = new Error();
        }
        notif.setTest("ch_xua_002_SubjectID");
        notif.setDescription("There MUST be one <Attribute> element with the name attribute  urn:oasis:names:tc:xspa:1.0:subject:subject-id . The <AttributeValue> child element MUST convey the subject’s real world name as plain text as defined by IHE XUA in all extensions.");
        notif.setLocation(location);
        notif.setIdentifiant("chxua-CHXUA_AssertionSpec-ch_xua_002_SubjectID");
        notif.getAssertions().add(new Assertion("CH-XUA", "CH-XUA-002"));
        diagnostic.add(notif);
    }

    private static void executeCons_CHXUA_AssertionSpec_Ch_xua_005_Role(net.ihe.gazelle.saml.assertion.AssertionType aClass,
                                                                        String location, List<Notification> diagnostic) {
        Notification notif = null;
        try {
            if (!(_validateCHXUA_AssertionSpec_Ch_xua_005_Role(aClass))) {
                notif = new Error();
            } else {
                notif = new Note();
            }
        } catch (Exception e) {
            notif = new Error();
        }
        notif.setTest("ch_xua_005_Role");
        notif.setDescription("The User Assertion MUST contain the attribute: /AttributeStatement/Attribute[@name=”urn:oasis:names:tc:xacml:2.0:subject:role”]. The AttributeValue MUST be PAT, HCP, DADM, PADM or REP for Representative");
        notif.setLocation(location);
        notif.setIdentifiant("chxua-CHXUA_AssertionSpec-ch_xua_005_Role");
        notif.getAssertions().add(new Assertion("CH-XUA", "CH-XUA-005"));
        diagnostic.add(notif);
    }

    private static void executeCons_CHXUA_AssertionSpec_Ch_xua_006_ResourceID(net.ihe.gazelle.saml.assertion.AssertionType aClass,
                                                                              String location, List<Notification> diagnostic) {
        Notification notif = null;
        try {
            if (!(_validateCHXUA_AssertionSpec_Ch_xua_006_ResourceID(aClass))) {
                notif = new Error();
            } else {
                notif = new Note();
            }
        } catch (Exception e) {
            notif = new Error();
        }
        notif.setTest("ch_xua_006_ResourceID");
        notif.setDescription("The User Assertion MUST contain the attribute: /AttributeStatement/Attribute[@name=”urn:oasis:names:tc:xacml:2.0:resource:resource-id”].");
        notif.setLocation(location);
        notif.setIdentifiant("chxua-CHXUA_AssertionSpec-ch_xua_006_ResourceID");
        notif.getAssertions().add(new Assertion("CH-XUA", "CH-XUA-006"));
        diagnostic.add(notif);
    }

    private static void executeCons_CHXUA_AssertionSpec_Ch_xua_007_PurposeOfUse(net.ihe.gazelle.saml.assertion.AssertionType aClass,
                                                                                String location, List<Notification> diagnostic) {
        Notification notif = null;
        try {
            if (!(_validateCHXUA_AssertionSpec_Ch_xua_007_PurposeOfUse(aClass))) {
                notif = new Error();
            } else {
                notif = new Note();
            }
        } catch (Exception e) {
            notif = new Error();
        }
        notif.setTest("ch_xua_007_PurposeOfUse");
        notif.setDescription("The User Assertion MUST contain the attribute: /AttributeStatement/Attribute[@name=”urn:oasis:names:tc:xspa:1.0:subject:purposeofuse”].");
        notif.setLocation(location);
        notif.setIdentifiant("chxua-CHXUA_AssertionSpec-ch_xua_007_PurposeOfUse");
        notif.getAssertions().add(new Assertion("CH-XUA", "CH-XUA-007"));
        diagnostic.add(notif);
    }

    private static void executeCons_CHXUA_AssertionSpec_Ch_xua_008_ResourceID_EPR_SPID(net.ihe.gazelle.saml.assertion.AssertionType aClass,
                                                                                       String location, List<Notification> diagnostic) {
        Notification notif = null;
        try {
            if (!(_validateCHXUA_AssertionSpec_Ch_xua_008_ResourceID_EPR_SPID(aClass))) {
                notif = new Warning();
            } else {
                notif = new Note();
            }
        } catch (Exception e) {
            notif = new Warning();
        }
        notif.setTest("ch_xua_008_ResourceID_EPR_SPID");
        notif.setDescription("The AttributeValue for ResourceID MUST be the EPR-PID of the patient, to which the transaction refers");
        notif.setLocation(location);
        notif.setIdentifiant("chxua-CHXUA_AssertionSpec-ch_xua_008_ResourceID_EPR_SPID");
        notif.getAssertions().add(new Assertion("CH-XUA", "CH-XUA-006"));
        diagnostic.add(notif);
    }

    private static void executeCons_CHXUA_AssertionSpec_Ch_xua_099_homeCommunityID(net.ihe.gazelle.saml.assertion.AssertionType aClass,
                                                                                   String location, List<Notification> diagnostic) {
        Notification notif = null;
        try {
            if (!(_validateCHXUA_AssertionSpec_Ch_xua_099_homeCommunityID(aClass))) {
                notif = new Error();
            } else {
                notif = new Note();
            }
        } catch (Exception e) {
            notif = new Error();
        }
        notif.setTest("ch_xua_099_homeCommunityID");
        notif.setDescription("There MUST be one <Attribute> element with the name attribute: urn:ihe:iti:xca:2010:homeCommunityId . The <AttributeValue> child element MUST convey the value of the Home Community ID (an Object Identifier) assigned to the Community that is initiating the request, using the urn format (that is, “urn:oid:” appended with the OID).");
        notif.setLocation(location);
        notif.setIdentifiant("chxua-CHXUA_AssertionSpec-ch_xua_099_homeCommunityID");

        diagnostic.add(notif);
    }

}
