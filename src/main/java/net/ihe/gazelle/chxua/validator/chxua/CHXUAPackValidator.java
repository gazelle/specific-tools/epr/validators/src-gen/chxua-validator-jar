package net.ihe.gazelle.chxua.validator.chxua;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;
import net.ihe.gazelle.validation.Notification;

import java.util.List;


public class CHXUAPackValidator implements ConstraintValidatorModule {


    /**
     * Create a new ObjectValidator that can be used to create new instances of schema derived classes for package: generated
     */
    public CHXUAPackValidator() {
    }


    /**
     * Validation of instance of an object
     */
    public void validate(Object obj, String location, List<Notification> diagnostic) {

        if (obj instanceof net.ihe.gazelle.saml.assertion.AssertionType) {
            net.ihe.gazelle.saml.assertion.AssertionType aClass = (net.ihe.gazelle.saml.assertion.AssertionType) obj;
            CHXUA_AssertionRoleASSValidator._validateCHXUA_AssertionRoleASS(aClass, location, diagnostic);
            CHXUA_AssertionRoleDADMValidator._validateCHXUA_AssertionRoleDADM(aClass, location, diagnostic);
            CHXUA_AssertionRoleHCPValidator._validateCHXUA_AssertionRoleHCP(aClass, location, diagnostic);
            CHXUA_AssertionRoleNotHCPValidator._validateCHXUA_AssertionRoleNotHCP(aClass, location, diagnostic);
            CHXUA_AssertionRolePADMValidator._validateCHXUA_AssertionRolePADM(aClass, location, diagnostic);
            CHXUA_AssertionRolePATValidator._validateCHXUA_AssertionRolePAT(aClass, location, diagnostic);
            CHXUA_AssertionRoleREPValidator._validateCHXUA_AssertionRoleREP(aClass, location, diagnostic);
            CHXUA_AssertionRoleTCUValidator._validateCHXUA_AssertionRoleTCU(aClass, location, diagnostic);
            CHXUA_AssertionSpecValidator._validateCHXUA_AssertionSpec(aClass, location, diagnostic);
        }

    }

}

