package net.ihe.gazelle.chxua.validator.chxua;

import net.ihe.gazelle.saml.assertion.AttributeStatementType;
import net.ihe.gazelle.saml.assertion.AttributeType;
import net.ihe.gazelle.saml.assertion.AttributeValueType;
import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import java.util.List;


/**
 * class :        CHXUA_AssertionRoleNotHCP
 * package :   chxua
 * Template Class
 * Template identifier :
 * Class of test : AssertionSpec
 */
public final class CHXUA_AssertionRoleNotHCPValidator {


    private CHXUA_AssertionRoleNotHCPValidator() {
    }


    /**
     * Validation of instance by a constraint : ch_xua_038_OrganizationId
     * There must be one organization ID attribute ( urn:oasis:names:tc:xspa:1.0:subject:organization-id ) element and it MUST be empty.
     */
    private static boolean _validateCHXUA_AssertionRoleNotHCP_Ch_xua_038_OrganizationId(net.ihe.gazelle.saml.assertion.AssertionType aClass) {
        Boolean result1;
        result1 = true;

        /* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
        try {
            for (AttributeStatementType anElement1 : aClass.getAttributeStatement()) {
                java.util.ArrayList<AttributeType> result2;
                result2 = new java.util.ArrayList<AttributeType>();

                /* Iterator Select: Select all elements which fulfill the condition. */
                try {
                    for (AttributeType anElement2 : anElement1.getAttribute()) {
                        if (((!(((String) anElement2.getName()) == null) && anElement2.getName().equals("urn:oasis:names:tc:xspa:1.0:subject:organization-id")) && tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.isEmpty(anElement2.getAttributeValue()))) {
                            result2.add(anElement2);
                        }
                        // no else
                    }
                } catch (Exception e) {
                }

                if (!((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(anElement1.getAttribute()) > new Integer(0)) && ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(new Integer(1)))) {
                    result1 = false;
                    break;
                }
                // no else
            }
        } catch (Exception e) {
        }

        return result1;


    }

    /**
     * Validation of instance by a constraint : ch_xua_039_Organization
     * There must be one organization attribute ( urn:oasis:names:tc:xspa:1.0:subject:organization ) element and it MUST be empty
     */
    private static boolean _validateCHXUA_AssertionRoleNotHCP_Ch_xua_039_Organization(net.ihe.gazelle.saml.assertion.AssertionType aClass) {
        Boolean result1;
        result1 = true;

        /* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
        try {
            for (AttributeStatementType anElement1 : aClass.getAttributeStatement()) {
                java.util.ArrayList<AttributeType> result2;
                result2 = new java.util.ArrayList<AttributeType>();

                /* Iterator Select: Select all elements which fulfill the condition. */
                try {
                    for (AttributeType anElement2 : anElement1.getAttribute()) {
                        if (((!(((String) anElement2.getName()) == null) && anElement2.getName().equals("urn:oasis:names:tc:xspa:1.0:subject:organization")) && tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.isEmpty(anElement2.getAttributeValue()))) {
                            result2.add(anElement2);
                        }
                        // no else
                    }
                } catch (Exception e) {
                }

                if (!((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(anElement1.getAttribute()) > new Integer(0)) && ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(new Integer(1)))) {
                    result1 = false;
                    break;
                }
                // no else
            }
        } catch (Exception e) {
        }

        return result1;


    }

    /**
     * Validation of template-constraint by a constraint : CHXUA_AssertionRoleNotHCP
     * Verify if an element can be token as a Template of type CHXUA_AssertionRoleNotHCP
     */
    private static boolean _isCHXUA_AssertionRoleNotHCPTemplate(net.ihe.gazelle.saml.assertion.AssertionType aClass) {
        Boolean result1;
        result1 = true;

        /* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
        try {
            for (AttributeStatementType anElement1 : aClass.getAttributeStatement()) {
                Boolean result2;
                result2 = false;

                /* Iterator Exists: Iterate and check, if any element fulfills the condition. */
                try {
                    for (AttributeType anElement2 : anElement1.getAttribute()) {
                        Boolean result3;
                        result3 = true;

                        /* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
                        try {
                            for (AttributeValueType anElement3 : anElement2.getAttributeValue()) {
                                if (!(((((!(anElement3.getRole() == null) && !(((String) anElement3.getRole().getCode()) == null)) && anElement3.getRole().getCode().equals("PADM")) || anElement3.getRole().getCode().equals("DADM")) || anElement3.getRole().getCode().equals("PAT")) || anElement3.getRole().getCode().equals("REP"))) {
                                    result3 = false;
                                    break;
                                }
                                // no else
                            }
                        } catch (Exception e) {
                        }

                        if ((((!(((String) anElement2.getName()) == null) && anElement2.getName().equals("urn:oasis:names:tc:xacml:2.0:subject:role")) && (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(anElement2.getAttributeValue()) > new Integer(0))) && result3)) {
                            result2 = true;
                            break;
                        }
                        // no else
                    }
                } catch (Exception e) {
                }

                if (!((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(anElement1.getAttribute()) > new Integer(0)) && result2)) {
                    result1 = false;
                    break;
                }
                // no else
            }
        } catch (Exception e) {
        }

        return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getAttributeStatement()) > new Integer(0)) && result1);


    }

    /**
     * Validation of class-constraint : CHXUA_AssertionRoleNotHCP
     * Verify if an element of type CHXUA_AssertionRoleNotHCP can be validated by CHXUA_AssertionRoleNotHCP
     */
    public static boolean _isCHXUA_AssertionRoleNotHCP(net.ihe.gazelle.saml.assertion.AssertionType aClass) {
        return _isCHXUA_AssertionRoleNotHCPTemplate(aClass);
    }

    /**
     * Validate as a template
     * name ::   CHXUA_AssertionRoleNotHCP
     * class ::  net.ihe.gazelle.saml.assertion.AssertionType
     */
    public static void _validateCHXUA_AssertionRoleNotHCP(net.ihe.gazelle.saml.assertion.AssertionType aClass, String location, List<Notification> diagnostic) {
        if (_isCHXUA_AssertionRoleNotHCP(aClass)) {
            executeCons_CHXUA_AssertionRoleNotHCP_Ch_xua_038_OrganizationId(aClass, location, diagnostic);
            executeCons_CHXUA_AssertionRoleNotHCP_Ch_xua_039_Organization(aClass, location, diagnostic);
        }
    }

    private static void executeCons_CHXUA_AssertionRoleNotHCP_Ch_xua_038_OrganizationId(net.ihe.gazelle.saml.assertion.AssertionType aClass,
                                                                                        String location, List<Notification> diagnostic) {
        Notification notif = null;
        try {
            if (!(_validateCHXUA_AssertionRoleNotHCP_Ch_xua_038_OrganizationId(aClass))) {
                notif = new Error();
            } else {
                notif = new Note();
            }
        } catch (Exception e) {
            notif = new Error();
        }
        notif.setTest("ch_xua_038_OrganizationId");
        notif.setDescription("There must be one organization ID attribute ( urn:oasis:names:tc:xspa:1.0:subject:organization-id ) element and it MUST be empty.");
        notif.setLocation(location);
        notif.setIdentifiant("chxua-CHXUA_AssertionRoleNotHCP-ch_xua_038_OrganizationId");
        notif.getAssertions().add(new Assertion("CH-XUA", "CH-XUA-038"));
        diagnostic.add(notif);
    }

    private static void executeCons_CHXUA_AssertionRoleNotHCP_Ch_xua_039_Organization(net.ihe.gazelle.saml.assertion.AssertionType aClass,
                                                                                      String location, List<Notification> diagnostic) {
        Notification notif = null;
        try {
            if (!(_validateCHXUA_AssertionRoleNotHCP_Ch_xua_039_Organization(aClass))) {
                notif = new Error();
            } else {
                notif = new Note();
            }
        } catch (Exception e) {
            notif = new Error();
        }
        notif.setTest("ch_xua_039_Organization");
        notif.setDescription("There must be one organization attribute ( urn:oasis:names:tc:xspa:1.0:subject:organization ) element and it MUST be empty");
        notif.setLocation(location);
        notif.setIdentifiant("chxua-CHXUA_AssertionRoleNotHCP-ch_xua_039_Organization");
        notif.getAssertions().add(new Assertion("CH-XUA", "CH-XUA-039"));
        diagnostic.add(notif);
    }

}
